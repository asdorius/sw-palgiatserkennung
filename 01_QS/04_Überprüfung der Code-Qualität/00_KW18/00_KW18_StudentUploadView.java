package de.dhbw.citeright.webapplication.view.pages;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.citeright.webapplication.controller.listener.FileUploadReceiver;
import de.dhbw.citeright.webapplication.controller.listener.URLUploadListener;
import de.dhbw.citeright.webapplication.view.layout.PreLoginLayout;
/**
 * the view to upload a document from a student with a tokencode
 *
 */
@SuppressWarnings("serial")
public class StudentUploadView extends PreLoginLayout{
	public static final String NAME = "studentupload";
	HorizontalLayout horizontalLayout;
	FileUploadReceiver uploadReceiver;
	private String tokencode;
	TextField nameTF;
	TextField emailTF;
	TextField urlTF;
	CheckBox agreementCB;
	
	public StudentUploadView() {
		super();
		horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		setUploadForm();
		setUpload();
		content.addComponent(horizontalLayout);
		
	}
	/**
	 * sets the elements for the upload of a file or a URL
	 */
	private void setUpload() {
		VerticalLayout uploadLayout = new VerticalLayout();
		uploadLayout.setSpacing(true);
		
		uploadLayout.addComponent(new Label("Sie können hier entweder eine Datei oder eine URL einreichen."));
				
		uploadReceiver = new FileUploadReceiver();
		Upload upload = new Upload("Bitte Datei auswählen", uploadReceiver);
		upload.addSucceededListener(uploadReceiver);
		upload.setButtonCaption("Dokument einreichen");
		uploadLayout.addComponent(upload);
		
		urlTF = new TextField("Url eingeben");
		urlTF.setWidth("350px");
		Button urlUploadButton = new Button("URL einreichen", new URLUploadListener(this));
		uploadLayout.addComponent(urlTF);
		uploadLayout.addComponent(urlUploadButton);
		
		horizontalLayout.addComponent(uploadLayout);
		
	}
	/**
	 * sets the form where the student has to give some informations about his person.
	 */
	private void setUploadForm() {
		FormLayout uploadForm = new FormLayout();
		nameTF = new TextField("Name: ");
		emailTF = new TextField("E-Mail: ");
		Label agreementText = new Label("Hiermit versichere ich blablalbla selbst erstellt blabla usw. Und alles richtig  zitiert und so weiter und so fort TestTestTest"); 
		agreementText.setWidth("350px");
		agreementCB = new CheckBox("Versicherung akzeptieren");
		uploadForm.setCaption("Tragen Sie hier die benötigten Informationen ein");
		uploadForm.setWidth("500px");
		nameTF.setRequired(true);
		emailTF.setRequired(true);
		agreementCB.setRequired(true);
		
		uploadForm.addComponent(nameTF);
		uploadForm.addComponent(emailTF);
		uploadForm.addComponent(agreementText);
		uploadForm.addComponent(agreementCB);
		
		horizontalLayout.addComponent(uploadForm);
		
	}

	/**
	 * gets called if a view did load
	 */
	public void enter(ViewChangeEvent event) {
		super.enter(event);
		tokencode = event.getParameters();
		if(!proofTokencode()) {
			Notification.show("Fehlerhafter Tokencode", Type.ERROR_MESSAGE);
			getUI().getNavigator().navigateTo("studenttoken");
		} else {
		Notification.show(tokencode, Type.ERROR_MESSAGE);
		}
	}
/**
 * 
 * @return returns if its a legal tokencode
 */
	private boolean proofTokencode() {
		if (tokencode.length() == 10) {
			//TODO mit Datenbank abgleichen
			return true;
		} else {
			return false;
		}
		
	}
	/**
	 * 
	 * @return the value of the textfield urlTF 
	 */
	public String getUploadURL() {
		return urlTF.getValue();
	}
	/**
	 * 
	 * @return true if all fields or checkboxes are filled out
	 */
	public boolean formFilledOut(){
		if (nameTF.getValue().equals("") || emailTF.getValue().equals("") || agreementCB.getValue().equals(false)) {
			return false;
		} else {
			return true;
		}
	}
	/**
	 * 
	 * @return the tokencode from the current upload
	 */
	public String getTokenCode() {
		return tokencode;
	}
}
