package de.dhbw.citeright.essentials.mail;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.User;

/**
 * Class that provides the functionality of sending mails in the context of the
 * Citeright-Application. The official CiteRight mailing account is used. This
 * functionality contains sending mails with status information, password
 * recovery, user activiation and every other context where a mail would be
 * helpful.
 * 
 * @author Torsten Hopf
 *
 */
public class MailingSingleton {

	private static MailingSingleton INSTANCE;

	private Properties properties;
	private final String fromEmail = "CiteRight.Notification@gmail.com";
	private final String password = "DHBWHorb2015";

	private MailingSingleton() {
		properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.port", "465");
		properties.put("mail.smtp.socketFactory.port", "465");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
	}

	/**
	 * Returns the current instance of the MailingSingleton and creates one
	 * before, if none exists. It is used for sending mails in the CiteRight
	 * context.
	 * 
	 * @return the current instance of the MalingSingleton
	 */
	public static MailingSingleton getInstance() {

		if (INSTANCE == null)
			INSTANCE = new MailingSingleton();

		return INSTANCE;
	}

	/**
	 * Sends a mail to the User's Email Adress, that contains his password.
	 * 
	 * @param user
	 *            the User that needs his email adress.
	 */
	public void sendPasswordToUser(User user) {
		Session session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		});
		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromEmail));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(user.getEmail()));
			message.setSubject("Ihr Password für CiteRight");

			final String emailText = "Anbei ihr Passwort: <br/> "
					+ user.getPassword();

			message.setContent(emailText, "text/html");

			Transport.send(message);

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}

	/**
	 * Sends an activation mail to the given user, after it's status changed
	 * from waiting for activation to an activated one.
	 * 
	 * @param user
	 *            the freshly activated user
	 */
	public void sendActivationMailToUser(User user) {
		Session session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		});
		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromEmail));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(user.getEmail()));
			message.setSubject("Ihre Aktivierung bei CiteRight");

			final String emailText = "Guten Tag "
					+ user.getFirstName()
					+ ", <br/>"
					+ "ein anderer Nutzer hat Sie für die CiteRight Application freigeschaltet! Von nun an können Sie sich unter"
					+ CiteRightConstants.ADDRESS_WEB_APP
					+ " anmelden!<br/>"
					+ "Dies ist eine automatisch generierte Email, bitte antworten Sie nicht darauf<br/>"
					+ "Viele Grüße,<br/>" + "Ihr CiteRight Team";

			message.setContent(emailText, "text/html");

			Transport.send(message);

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}

	/**
	 * Sends an Email after the document process has finished to the User
	 * 
	 * @param user
	 *            the User that should receive the mail
	 * @param document
	 *            the document that ran through the process
	 */
	public void sendMailWithDocumentStatusFinished(User user, Document document) {

	}

}