CREATE DATABASE  IF NOT EXISTS `CiteRightSandbox` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `CiteRightSandbox`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: 185.82.20.252    Database: CiteRightSandbox
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Access`
--

DROP TABLE IF EXISTS `Access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Access` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `User_ID` int(11) DEFAULT NULL,
  `Room_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `User_idx` (`User_ID`),
  KEY `Folder_idx` (`Room_ID`),
  CONSTRAINT `Room` FOREIGN KEY (`Room_ID`) REFERENCES `Room` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `User` FOREIGN KEY (`User_ID`) REFERENCES `User` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `Action`
--

DROP TABLE IF EXISTS `Action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Action` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Name_UNIQUE` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Action`
--

LOCK TABLES `Action` WRITE;
/*!40000 ALTER TABLE `Action` DISABLE KEYS */;
INSERT INTO `Action` VALUES (2,'Activate_User'),(1,'Create_Folder'),(3,'Delete_User'),(4,'Update_User');
/*!40000 ALTER TABLE `Action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ActionRole`
--

DROP TABLE IF EXISTS `ActionRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ActionRole` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Action_ID` int(11) NOT NULL,
  `Role_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Action_ID_idx` (`Action_ID`),
  KEY `Role_ID_idx` (`Role_ID`),
  CONSTRAINT `Action_ID` FOREIGN KEY (`Action_ID`) REFERENCES `Action` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Role_ID` FOREIGN KEY (`Role_ID`) REFERENCES `Role` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ActionRole`
--

LOCK TABLES `ActionRole` WRITE;
/*!40000 ALTER TABLE `ActionRole` DISABLE KEYS */;
INSERT INTO `ActionRole` VALUES (1,1,1),(2,1,2),(3,2,1),(4,3,1),(5,4,1);
/*!40000 ALTER TABLE `ActionRole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Document`
--

DROP TABLE IF EXISTS `Document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Document` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Filename` varchar(255) DEFAULT NULL,
  `Filetype` varchar(45) DEFAULT NULL,
  `UploadDate` timestamp NULL DEFAULT NULL,
  `Parent` int(11) NOT NULL,
  `Reference` int(11) DEFAULT NULL,
  `data` longblob,
  `studentName` varchar(45) DEFAULT NULL,
  `studentEmail` varchar(255) DEFAULT NULL,
  `studentMatriculationNumber` varchar(45) DEFAULT NULL COMMENT 'matriculation number of jan',
  `plagiarismDegree` float DEFAULT NULL COMMENT '100% Default, Leerdammer 30% Fettanteil',
  `status` int(11) DEFAULT '5',
  `auditReport` longblob,
  `isForeignSource` bit(1) DEFAULT b'0',
  PRIMARY KEY (`ID`),
  KEY `Parent_idx` (`Parent`),
  KEY `Reference_idx` (`Reference`),
  KEY `Status_idx` (`status`),
  CONSTRAINT `Parent` FOREIGN KEY (`Parent`) REFERENCES `Room` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Reference` FOREIGN KEY (`Reference`) REFERENCES `Reference` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DocumentContent`
--

DROP TABLE IF EXISTS `DocumentContent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DocumentContent` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `File` int(11) NOT NULL COMMENT 'Represents the Text Content of a File',
  PRIMARY KEY (`ID`),
  KEY `fk_Document_1_idx` (`File`),
  CONSTRAINT `fk_Document_1` FOREIGN KEY (`File`) REFERENCES `Document` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `DocumentStatus`
--

DROP TABLE IF EXISTS `DocumentStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DocumentStatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `status_UNIQUE` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DocumentStatus`
--

LOCK TABLES `DocumentStatus` WRITE;
/*!40000 ALTER TABLE `DocumentStatus` DISABLE KEYS */;
INSERT INTO `DocumentStatus` VALUES (2,'In Warteschlange'),(5,'Unbekannt'),(1,'Wird geprüft'),(3,'Überprüfung abgeschlossen'),(4,'Überprüfung fehlgeschlagen');
/*!40000 ALTER TABLE `DocumentStatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `IncludedRoom`
--

DROP TABLE IF EXISTS `IncludedRoom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IncludedRoom` (
  `id` int(11) NOT NULL,
  `RoomId` int(11) NOT NULL,
  `IncludedRoomId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `Language`
--

DROP TABLE IF EXISTS `Language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Language` (
  `name` enum('German','English','French') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Language`
--

LOCK TABLES `Language` WRITE;
/*!40000 ALTER TABLE `Language` DISABLE KEYS */;
/*!40000 ALTER TABLE `Language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PossibleMatch`
--

DROP TABLE IF EXISTS `PossibleMatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PossibleMatch` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Section1` int(11) NOT NULL,
  `Section2` int(11) NOT NULL,
  `Measurement` float NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Sec1_idx` (`Section1`),
  KEY `Sec2_idx` (`Section2`),
  CONSTRAINT `Sec1` FOREIGN KEY (`Section1`) REFERENCES `Section` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Sec2` FOREIGN KEY (`Section2`) REFERENCES `Section` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PossibleMatch`
--

LOCK TABLES `PossibleMatch` WRITE;
/*!40000 ALTER TABLE `PossibleMatch` DISABLE KEYS */;
/*!40000 ALTER TABLE `PossibleMatch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Reference`
--

DROP TABLE IF EXISTS `Reference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Reference` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Adress` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Role`
--

DROP TABLE IF EXISTS `Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Role` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Name_UNIQUE` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Role`
--

LOCK TABLES `Role` WRITE;
/*!40000 ALTER TABLE `Role` DISABLE KEYS */;
INSERT INTO `Role` VALUES (1,'Administrator'),(2,'Dozent'),(3,'Waiting');
/*!40000 ALTER TABLE `Role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Room`
--

DROP TABLE IF EXISTS `Room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Room` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Token` varchar(45) NOT NULL,
  `Owner` int(11) NOT NULL,
  `Parent` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Owner_idx` (`Owner`),
  CONSTRAINT `Owner` FOREIGN KEY (`Owner`) REFERENCES `User` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Room`
--

LOCK TABLES `Room` WRITE;
/*!40000 ALTER TABLE `Room` DISABLE KEYS */;
INSERT INTO `Room` VALUES (2,'root','xkcd',1,NULL),(10,'websites','Dck49dqE5D',1,1);
/*!40000 ALTER TABLE `Room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Section`
--

DROP TABLE IF EXISTS `Section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Section` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Document_ID` int(11) NOT NULL,
  `Headword` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `OrderingNumber` int(11) NOT NULL,
  `SectionText` varchar(6000) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Document_idx` (`Document_ID`),
  CONSTRAINT `fk_Document` FOREIGN KEY (`Document_ID`) REFERENCES `DocumentContent` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(255) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Role` int(11) NOT NULL,
  `lowerThreshold` int(3) DEFAULT '50',
  `upperThreshold` int(3) DEFAULT '75',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Email_UNIQUE` (`Email`),
  KEY `UserRole_idx` (`Role`),
  CONSTRAINT `UserRole` FOREIGN KEY (`Role`) REFERENCES `Role` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

-- HAVE FUN!!
