package com.example.demo_application;

import java.io.File;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Theme("demo_application")
public class Demo_applicationUI extends UI {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = Demo_applicationUI.class)
	public static class Servlet extends VaadinServlet {
	}
	
	private VerticalLayout content;
	
	@Override
	protected void init(VaadinRequest request) {
		// background layout
		VerticalLayout layout = new VerticalLayout();
		// avoid margin
		layout.setMargin(false);
		// Set the layout as the content of the UI
		// The UI is the main part of our application
		setContent(layout);
		
		// Let's build a menu
		MenuBar menu = new MenuBar();
		//let it use the whole screen width
		menu.setSizeFull();
		// stretch it's height
		menu.setHeight(40, Unit.PIXELS);
		
		// menu.addItem returns a MenuItem
		MenuItem uploadItem = menu.addItem("Datei hochladen", new UploadCommand());
		
		// Sample for resources
		FileResource uploadResource = new FileResource(new File(VaadinService
				.getCurrent().getBaseDirectory().getAbsolutePath()
				+ "/WEB-INF/icons/upload-icon-small.png"));
		// Set the Icon
		uploadItem.setIcon(uploadResource);
		
		
		
		MenuItem showItem = menu.addItem("Dateien", new ShowFilesCommand());
		FileResource filesResource = new FileResource(new File(VaadinService
				.getCurrent().getBaseDirectory().getAbsolutePath()
				+ "/WEB-INF/icons/note-icon-small.png"));
		showItem.setIcon(filesResource);
		
		
		SettingsCommand settingsCommand = new SettingsCommand();
		MenuItem settingsItem = menu.addItem("�berpr�fe auf Plagiate", settingsCommand);
		FileResource settingsResource = new FileResource(new File(VaadinService
				.getCurrent().getBaseDirectory().getAbsolutePath()
				+ "/WEB-INF/icons/zoom-in-icon-small.png"));
		settingsItem.setIcon(settingsResource);
		
		MenuItem profileItem = menu.addItem("Profil",null,null);
		FileResource profileResource = new FileResource(new File(VaadinService
				.getCurrent().getBaseDirectory().getAbsolutePath()
				+ "/WEB-INF/icons/java-small.png"));
		profileItem.setIcon(profileResource);
		
		
		profileItem.addItem("Profil bearbeiten", null,null);
		profileItem.addItem("Statistik",null,null);
		
		Label heading = new Label("Hello Vaadin!");
		heading.setStyleName("h1");
		
		content = new VerticalLayout(heading);
		content.setSizeFull();
		content.setSpacing(true);
		//add it to layout
		layout.addComponent(menu);
		layout.addComponent(content);
		
		
		
	}

	public VerticalLayout getContentLayout() {
		return content;
	}

	public void setContentLayout(VerticalLayout content) {
		this.content = content;
	}

}