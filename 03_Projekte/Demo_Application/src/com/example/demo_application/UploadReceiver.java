package com.example.demo_application;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.vaadin.server.VaadinService;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;

@SuppressWarnings("serial")
public class UploadReceiver implements Receiver, SucceededListener,
		FailedListener {

	@Override
	public void uploadFailed(FailedEvent event) {
		Notification.show("Could not upload file!", Type.ERROR_MESSAGE);
	}

	@Override
	public void uploadSucceeded(SucceededEvent event) {
		Notification.show("Sucessfully uploaded file!");

	}

	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {
		File uploadedFile = new File(VaadinService.getCurrent()
				.getBaseDirectory().getAbsolutePath()
				+ "/WEB-INF/uploads/" + filename);
		

		try {
			FileOutputStream fos = new FileOutputStream(uploadedFile);
			BufferedOutputStream outputStream = new BufferedOutputStream(fos);
			return outputStream;
		} catch (FileNotFoundException e) {
			Notification.show("Could not upload file!", Type.ERROR_MESSAGE);
			e.printStackTrace();
			return null;
		}

	}

}
