package com.example.demo_application;

import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;

@SuppressWarnings("serial")
public class UploadCommand implements Command {

	@Override
	public void menuSelected(MenuItem selectedItem) {
		System.out.println(selectedItem.getText());
		// Vaadin has an own upload component
		Upload upload = new Upload();
		upload.setCaption("Upload");
		upload.setStyleName("h1");

		// The Upload component has to be connected to an Receiver interface
		// My implementation implements also the failed and succeeded Listener
		UploadReceiver receiver = new UploadReceiver();

		upload.setReceiver(receiver);
		upload.addFailedListener(receiver);
		upload.addSucceededListener(receiver);
		((Demo_applicationUI) UI.getCurrent()).getContentLayout()
				.removeAllComponents();
		((Demo_applicationUI) UI.getCurrent()).getContentLayout().addComponent(
				upload);
	}

}
