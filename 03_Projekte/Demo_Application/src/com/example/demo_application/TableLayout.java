package com.example.demo_application;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.util.Iterator;
import java.util.stream.Stream;

import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class TableLayout extends VerticalLayout {

	public TableLayout() {
		try {
			this.addComponent(buildFileTable());
		} catch (IOException e) {
			e.printStackTrace();
			Notification.show("Error on building Table", Type.ERROR_MESSAGE);
		}
	}

	@SuppressWarnings("unchecked")
	public Table buildFileTable() throws IOException {

		// Container is one of the main data structures in Vaadin
		// It implements the Interface Datasource
		IndexedContainer container = new IndexedContainer();
		Object filenameProperty = "Filename";
		Object dateProperty = "Date";
		Object removeProperty = "Remove";
		
		// Add container properties (columns)
		container.addContainerProperty(filenameProperty, String.class, "");
		container.addContainerProperty(dateProperty, String.class, "");
		container.addContainerProperty(removeProperty, Component.class, null);

		String path = VaadinService.getCurrent().getBaseDirectory()
				.getAbsolutePath()
				+ File.separator + "WEB-INF" + File.separator + "Uploads";

		// Some file handling ...
		Path filePath = Paths.get(path);
		if(filePath == null) return null;
		Stream<Path> stream = Files.list(filePath);
		if(stream == null) return null;

		Iterator<Path> iterator = stream.iterator();
		// more file handling....
		while (iterator.hasNext()) {
			Path nextPath = iterator.next();
			File file = nextPath.toFile();
			String filename = file.getName();
			String date = new Date(file.lastModified()).toString();
			Button removeButton = new Button("Remove");
			// enough file handling...
			
			//container.addItem() returns the ID as Object 
			Object id = container.addItem();
			
			// add the properties to the new Item / set values for columns
			// not very handy, but who cares?
			container.getItem(id).getItemProperty(filenameProperty)
					.setValue(filename);
			container.getItem(id).getItemProperty(dateProperty).setValue(date);
			container.getItem(id).getItemProperty(removeProperty)
					.setValue(removeButton);
			
			

		}
		stream.close();
		// link the table with the datasource
		Table table = new Table("Files", container);
		table.setWidth(100, Unit.PERCENTAGE);
		table.setStyleName("h1");
		return table;

	}
}
