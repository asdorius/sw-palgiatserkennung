package com.example.demo_application;

import com.vaadin.ui.UI;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
@SuppressWarnings("serial")
public class ShowFilesCommand implements Command {

	@Override
	public void menuSelected(MenuItem selectedItem) {
		((Demo_applicationUI) UI.getCurrent()).getContentLayout().removeAllComponents();
		((Demo_applicationUI) UI.getCurrent()).getContentLayout().addComponent(new TableLayout());
	}

}
