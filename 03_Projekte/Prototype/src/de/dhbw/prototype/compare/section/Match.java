package de.dhbw.prototype.compare.section;

public class Match {
	protected Section section1;
	protected Section section2;
	
	protected double measurement;
	
	public Match(Section section1, Section section2, double measurement){
		this.section1 = section1;
		this.section2 = section2;
		this.measurement = measurement;
	}
	
	public double getMeasurement(){
		return measurement;
	}
	
	public String toString(){
		return "{\"section1\" = \""+section1.toString()+"\", \"section2\" = \""+section2.toString()+"\", \"measurement\" = \""+measurement+"\"}";
	}

	public Section getSection1() {
		return section1;
	}

	public Section getSection2() {
		return section2;
	}
}
