package de.dhbw.prototype.compare.section.converter;

import java.util.LinkedList;
import java.util.List;

import de.dhbw.prototype.compare.section.BaseSection;
import de.dhbw.prototype.compare.section.Section;

public class LineSectionsConverter implements TextToSectionsConverter {
	@Override
	public List<Section> convert(String text) {
		List<Section> sections = new LinkedList<>();
		
		String lineSeperator = "[\\r\\n]";
		
		String[] parts = text.split(lineSeperator);
		int index = 0;
		
		for(String part: parts){
			sections.add(new BaseSection(part, index));
			index += part.length();
		}
		return sections;
	}
}
