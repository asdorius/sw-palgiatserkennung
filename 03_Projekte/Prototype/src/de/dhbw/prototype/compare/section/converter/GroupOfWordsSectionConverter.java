package de.dhbw.prototype.compare.section.converter;

import java.util.LinkedList;
import java.util.List;

import de.dhbw.prototype.compare.section.BaseSection;
import de.dhbw.prototype.compare.section.Section;

public class GroupOfWordsSectionConverter implements TextToSectionsConverter{
	private int maxCountOfWords = 3;

	@Override
	public List<Section> convert(String text) {
		String[] words = text.split(" ");
		List<Section> sections = new LinkedList<Section>();
		
		String groupOfWords = "";
		int countOfWords = 0;
		int index = 0;
		
		for(String word: words){
			if(countOfWords > 0){
				groupOfWords += " "+word;
			}else{
				groupOfWords += word;
			}
			
			countOfWords++;
			
			if(countOfWords >= maxCountOfWords){
				index += groupOfWords.length();
				
				sections.add(new BaseSection(groupOfWords, index));
				
				countOfWords = 0;
				groupOfWords = "";
			}
		}
		
		if(countOfWords > 0){
			index += groupOfWords.length();
			
			sections.add(new BaseSection(groupOfWords, index));
		}
		
		return sections;
	}

}
