package de.dhbw.prototype.compare.section;

public class BaseSection implements Section{
	private String text;
	private int index = -1;
	
	public BaseSection(String text, int index){
		this.text = text;
		this.index = index;
	}
	
	/**
	 * Returns only 0 or 1
	 * @see Section
	 */
	public double compare(Section section){
		if(text.equals(section.getText())){
			return 1;
		}
		return 0;
	}

	@Override
	public String getText(){
		return text;
	}
	
	@Override
	public void setText(String text) {
		this.text = text;
	}
	
	public String toString(){
		return "{text: "+text+";index: "+index+"}";
	}

	@Override
	public int getIndex() {
		return index;
	}

	@Override
	public void setIndex(int index) {
		this.index = index;
	}
}
