package de.dhbw.prototype.compare.section.converter;

import java.util.List;

import de.dhbw.prototype.compare.section.BaseSection;
import de.dhbw.prototype.compare.section.Section;

public interface TextToSectionsConverter {
	public List<Section> convert(String text);
}
