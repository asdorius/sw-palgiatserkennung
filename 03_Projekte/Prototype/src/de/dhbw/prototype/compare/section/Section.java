package de.dhbw.prototype.compare.section;

public interface Section {	
	/**
	 * Compares a given Section with the current Section
	 * @param section
	 * @return double between 0 and 1
	 */
	public double compare(Section section);
	
	public String getText();
	public void setText(String text);
	
	public int getIndex();
	public void setIndex(int index);
}
