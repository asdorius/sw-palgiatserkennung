package de.dhbw.prototype.compare.section;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CompareWordsSection extends SectionProxy{

	public CompareWordsSection(Section section) {
		super(section);
	}
	
	@Override
	public double compare(Section section) {
		String currentString = getText();
		String givenString = section.getText();
		
		String[] currentStringWords = currentString.split(" ");
		String[] givenStringWords = givenString.split(" ");
		
		List<String> currentStringWordList =  new ArrayList<String>(Arrays.asList(currentStringWords));
		List<String> givenStringWordList =  new ArrayList<String>(Arrays.asList(givenStringWords));
		
		currentStringWordList.retainAll(givenStringWordList);
		
		return (2.0 * currentStringWordList.size())/(currentStringWords.length+givenStringWords.length);
	}
}
