package de.dhbw.prototype.compare.section;

public class SectionProxy implements Section {
	protected Section section;
	
	public SectionProxy(Section section){
		this.section = section;
	}
	

	@Override
	public double compare(Section section) {
		return this.section.compare(section);
	}

	@Override
	public String getText() {
		return section.getText();
	}


	@Override
	public void setText(String text) {
		section.setText(text);
	}
	
	public String toString(){
		return section.toString();
	}


	@Override
	public int getIndex() {
		return section.getIndex();
	}


	@Override
	public void setIndex(int index) {
		section.setIndex(index);
	}
}
