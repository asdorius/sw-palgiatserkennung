package de.dhbw.prototype.compare.section.converter;

import java.util.LinkedList;
import java.util.List;

import de.dhbw.prototype.compare.section.BaseSection;
import de.dhbw.prototype.compare.section.Section;

public class SentenceSectionConverter implements TextToSectionsConverter {

	@Override
	public List<Section> convert(String text) {
		List<Section> sections = new LinkedList<>();
		
		String[] parts = text.split("\\.");
		
		int index = 0;
		
		for(String part: parts){
			if(parts.length > 0){
				sections.add(new BaseSection(part, index));
				index += part.length()+1;
			}
		}
		return sections;
	}
}
