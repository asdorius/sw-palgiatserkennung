package de.dhbw.prototype.file;

import java.net.*;
import java.util.List;
import java.io.*;

import net.htmlparser.jericho.*;
import de.dhbw.prototype.search.*;

public class HTMLtoTextConverter {
	/**
	 * Takes the sourcecode from an URL and converts it to an textfile.
	 * 
	 * @param phrase
	 *            Phrase for which should be searched
	 * @throws IOException 
	 */
	public static void convertHTMLToText(String phrase) throws IOException {
		SearchEngine googleSearchEngine = new GoogleSearchEngine();
		
		List<SearchResult> results = googleSearchEngine.search(phrase);
		String filename;

		String title;
		PrintWriter output = null;
		Source htmlSource;
		Segment htmlSeg;
		Renderer htmlRend = null;
		
		//TODO: Problems with some of the htmlSource-requests
		for (SearchResult item : results) {
			filename = (generateFilename(item.getUrl()));

			Config.LoggerProvider = LoggerProvider.DISABLED;

			try {
				htmlSource = new Source(new URL(item.getUrl()));
				title = getTitle(htmlSource);
				htmlSeg = new Segment(htmlSource, 0, htmlSource.length());
				htmlRend = new Renderer(htmlSeg);
				output = new PrintWriter("files/" + filename);
				output.println(title);
				output.println(htmlRend.toString());

			} catch (IOException e) {
				System.out.println("Die URL wurde nicht gefunden");
				e.printStackTrace();
			} finally {
				output.close();
			}
		}

	}

	/**
	 * Extract the title out of the HTML Source.
	 * 
	 * @param htmlSource
	 *            is the sourcecode of the website
	 * @return returns the title of the website
	 */
	private static String getTitle(Source htmlSource) {
		Element titleElement = htmlSource
				.getFirstElement(HTMLElementName.TITLE);
		if (titleElement == null)
			return null;
		return CharacterReference.decodeCollapseWhiteSpace(titleElement
				.getContent());
	}

	/**
	 * Generates the filename for the txt-file created from a website. It uses
	 * the URL from this website.
	 * 
	 * @param urlText
	 *            is the URL from the website
	 * @return returns the created filename
	 */
	private static String generateFilename(String urlText) {
		String filename = urlText;
		filename = filename.replace("/", "");
		filename = filename.replace(":", "");
		filename = filename.replace("?", "");
		filename = filename + ".txt";
		return filename;
	}

}
