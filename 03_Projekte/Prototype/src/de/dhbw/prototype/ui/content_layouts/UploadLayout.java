package de.dhbw.prototype.ui.content_layouts;

import java.io.OutputStream;

import com.vaadin.ui.Label;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.prototype.ui.UploadReceiver;

public class UploadLayout extends VerticalLayout{
	public UploadLayout(){
		addComponent(new Label("UploadLayout"));
		UploadReceiver receiver = new UploadReceiver();
		Upload fileUpload = new Upload("", receiver);
		fileUpload.addSucceededListener(receiver);
		addComponent(fileUpload);
		

	}
}
