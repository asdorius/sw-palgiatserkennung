package de.dhbw.prototype.ui.content_layouts;

import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;

public class FileViewerLayout extends VerticalLayout{
	private TextArea textarea;

	public FileViewerLayout(){
		textarea = new TextArea();
		textarea.setSizeFull();
		addComponent(textarea);
	}
}
