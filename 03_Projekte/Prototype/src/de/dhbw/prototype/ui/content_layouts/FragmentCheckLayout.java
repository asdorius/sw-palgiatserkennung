package de.dhbw.prototype.ui.content_layouts;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class FragmentCheckLayout extends VerticalLayout{
	private TextArea textarea;
	private Button checkButton;
	private Table resultTable;
	private IndexedContainer resultContainer;

	public FragmentCheckLayout(){
		this.setSizeFull();
		
		TextArea textarea= new TextArea();
		textarea.setSizeFull();
		checkButton = new Button("Überprüfen");
		
		resultTable = new Table();
		resultTable.setWidth(100, Unit.PERCENTAGE);
		
		resultContainer = new IndexedContainer();
		resultContainer.addContainerProperty("Title", String.class, "");
		resultContainer.addContainerProperty("Link", String.class, "");
		
		//TODO: remove Dummy Data
		Item first = resultContainer.addItem("first");
		first.getItemProperty("Title").setValue("TITLE");
		first.getItemProperty("Link").setValue("LINK");
		
		Item second = resultContainer.addItem("second");
		second.getItemProperty("Title").setValue("TITLE");
		second.getItemProperty("Link").setValue("LINK");
		
		Item third = resultContainer.addItem("third");
		third.getItemProperty("Title").setValue("TITLE");
		third.getItemProperty("Link").setValue("LINK");
		
		
		resultTable.setContainerDataSource(resultContainer);
		
		addComponent(textarea);
		addComponent(checkButton);
		addComponent(resultTable);
		
	}
}
