package de.dhbw.prototype.ui.content_layouts;

import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

public class LoginLayout extends VerticalLayout {
	private final String header = "Login";
	private TextField userField;
	private PasswordField passwordField;
	private Button loginButton;
	
	public LoginLayout(){
		userField = new TextField("Email");
		passwordField = new PasswordField("Passwort");
		loginButton = new Button("Login");
		loginButton.addClickListener(new Button.ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				//TODO: Login-Handling
				if(userField.getValue()!="" && passwordField.getValue()!=""){
				addComponent(new Label(userField.getValue() + ", you're logged in!"));	
				}
			}
		});
		
		addComponent(userField);
		addComponent(passwordField);
		addComponent(loginButton);
	}

}
