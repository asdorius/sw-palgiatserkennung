package de.dhbw.prototype.ui.content_layouts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

import de.dhbw.prototype.compare.section.BaseSection;
import de.dhbw.prototype.compare.section.CompareWordsSection;
import de.dhbw.prototype.compare.section.Match;
import de.dhbw.prototype.compare.section.Section;
import de.dhbw.prototype.compare.section.converter.GroupOfWordsSectionConverter;
import de.dhbw.prototype.compare.section.converter.LineSectionsConverter;
import de.dhbw.prototype.compare.section.converter.SentenceSectionConverter;
import de.dhbw.prototype.compare.section.converter.TextToSectionsConverter;

public class CompareLayout extends GridLayout{
	private TextArea textAreaLeft;
	private TextArea textAreaRight;
	
	public CompareLayout(){
		super(2, 1);
		this.setSizeFull();
		
		textAreaLeft = new TextArea("Links");
		textAreaLeft.setSizeFull();
		
		textAreaRight = new TextArea("Rechts");
		textAreaRight.setSizeFull();
		
		VerticalLayout left = new VerticalLayout();
		left.setMargin(true);
		left.addComponent(textAreaLeft);
		addComponent(left);

		
		VerticalLayout right = new VerticalLayout();
		right.setMargin(true);
		right.addComponent(textAreaRight);
		addComponent(right);
		
		textAreaLeft.setValue("A B C D E F G H I J K L M N O P Q R S T U V W X Y Z");
		textAreaRight.setValue("Q W E R T Z U I O P A S D F G H J K L Y X C V B N M");
		
		final Container resultContainer = new IndexedContainer();
		resultContainer.addContainerProperty("Section1" , String.class, "");
		resultContainer.addContainerProperty("Section2" , String.class, "");
		resultContainer.addContainerProperty("Measurement" , Double.class, 0);
		
		Button searchButton = new Button("Suchen", new Button.ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				TextToSectionsConverter converter = new GroupOfWordsSectionConverter();
				
				List<Section> sections1 = converter.convert(textAreaLeft.getValue());
				List<Section> sections2 = converter.convert(textAreaRight.getValue());
				
				double barrier = 0.5;
				
				Map<Section, Match> results = new HashMap<Section, Match>();
				
				for(Section section1: sections1){
					for(Section section2: sections2){
						double measurement = new CompareWordsSection(section1).compare(section2);
						if(measurement > barrier && (!results.containsKey(section1) || results.get(section1).getMeasurement() < measurement)){
							results.put(section1, new Match(section1, section2, measurement));
						}
					}
				}
				
				System.out.println(results);
				System.out.println(results.size());
				
				Set<Section> resultKeys = results.keySet();
				
				resultContainer.removeAllItems();
				
				for(Section key: resultKeys){
					Match match = results.get(key);
					
					Item item = resultContainer.addItem(key.hashCode());
					item.getItemProperty("Section1").setValue(match.getSection1().getText());
					item.getItemProperty("Section2").setValue(match.getSection2().getText());
					item.getItemProperty("Measurement").setValue(match.getMeasurement());
				}
			}
		});
		
		addComponent(searchButton);
		
		Table resultTable = new Table();
		resultTable.setSizeFull();
		
		resultTable.setContainerDataSource(resultContainer);
		
		addComponent(resultTable);
	}
	
	private String getDummyData2() {
		return "Lorem ipsum";
	}

	private String getDummyData1(){
		return "Lorem ipsum dolor sit amet, consetetur sadipscing elitr,\n sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,\n sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.\n Lorem ipsum dolor sit amet,\n consetetur sadipscing elitr,\n sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,\n sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur";
	}
}
