package de.dhbw.prototype.ui;

import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;

import de.dhbw.prototype.ui.content_layouts.CompareLayout;
import de.dhbw.prototype.ui.content_layouts.FileViewerLayout;
import de.dhbw.prototype.ui.content_layouts.FragmentCheckLayout;
import de.dhbw.prototype.ui.content_layouts.LoginLayout;
import de.dhbw.prototype.ui.content_layouts.SourceCheckLayout;
import de.dhbw.prototype.ui.content_layouts.UploadLayout;

public class MenuCommand implements Command{
	private PrototypeUI ui;

	public MenuCommand(PrototypeUI ui){
		this.ui = ui;
	}

	@Override
	public void menuSelected(MenuItem selectedItem) {
		String text = selectedItem.getText();
		
		switch(text){  
			case "Upload":
				ui.setContentLayout(new UploadLayout());
			break;
			case "Datei Viewer":
				ui.setContentLayout(new FileViewerLayout());
			break;
			case "Fragmentpr�fung":
				ui.setContentLayout(new FragmentCheckLayout());
			break;
			case "Vergleich":
				ui.setContentLayout(new CompareLayout());
			break;
			case "Quellpr�fung":
				ui.setContentLayout(new SourceCheckLayout());
			break;
			case "Login":
				ui.setContentLayout(new LoginLayout());
		}
			
	}

}
