package de.dhbw.prototype.ui;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;

import de.dhbw.prototype.ui.content_layouts.FileViewerLayout;

public class UploadReceiver implements Upload.Receiver, SucceededListener {
	ByteArrayOutputStream byteArrayOutputStream = null;
	String uploadedText;
	String filename;
	String mimeType;

	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {
		this.filename = filename;
		this.mimeType = mimeType;
		byteArrayOutputStream = new ByteArrayOutputStream();
		String test = byteArrayOutputStream.toString();
		System.out.println(test);
		return byteArrayOutputStream;
	}

	@Override
	public void uploadSucceeded(SucceededEvent event) {
		switch (mimeType) {
		case "text/plain":
			uploadedText = byteArrayOutputStream.toString();
			new Notification("TEST", uploadedText ,Notification.Type.HUMANIZED_MESSAGE).show(Page.getCurrent());
			
			break;
		default:
			new Notification("Fehler", "Der ausgewählte Dateityp wird momentan noch nicht unterstützt" ,Notification.Type.ERROR_MESSAGE).show(Page.getCurrent());
			break;
		}
	}
	public String getUploadedText() {
		return uploadedText;
	}

}
