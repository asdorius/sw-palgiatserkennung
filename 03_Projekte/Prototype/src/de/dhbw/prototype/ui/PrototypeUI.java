package de.dhbw.prototype.ui;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.prototype.ui.content_layouts.CompareLayout;
import de.dhbw.prototype.ui.content_layouts.FileViewerLayout;
import de.dhbw.prototype.ui.content_layouts.FragmentCheckLayout;
import de.dhbw.prototype.ui.content_layouts.SourceCheckLayout;
import de.dhbw.prototype.ui.content_layouts.UploadLayout;

@SuppressWarnings("serial")
@Theme("prototype")
public class PrototypeUI extends UI {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = PrototypeUI.class)
	public static class Servlet extends VaadinServlet {
	}

	private VerticalLayout content;

	@Override
	protected void init(VaadinRequest request) {
		VerticalLayout layout = new VerticalLayout();
		layout.setMargin(false);
		setContent(layout);
		
		MenuBar menu = new MenuBar();
		menu.setWidth(100, Unit.PERCENTAGE);
		
		menu.addItem("Upload", new MenuCommand(this));
		menu.addItem("Datei Viewer", new MenuCommand(this));
		menu.addItem("Fragmentpr�fung", new MenuCommand(this));
		menu.addItem("Vergleich", new MenuCommand(this));
		menu.addItem("Quellpr�fung", new MenuCommand(this));
		menu.addItem("Login", new MenuCommand(this));
		
		layout.addComponent(menu);
		
		content = new VerticalLayout();
		content.setMargin(true);
		layout.addComponent(content);
		
		Button button = new Button("Click Me");
		button.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				content.addComponent(new Label("Thank you for clicking"));
			}
		});
		setContentLayout(button);
	}
	
	public void setContentLayout(Component content){
		this.content.removeAllComponents();
		this.content.addComponent(content);
	}

}