package de.dhbw.prototype.db;

import java.sql.Connection;
import java.sql.SQLException;

import com.vaadin.data.util.sqlcontainer.connection.SimpleJDBCConnectionPool;

public class DataBaseUtil {

	private static SimpleJDBCConnectionPool pool = null;

	public static Connection getConnection() {
		Connection cn = null;
		try {
			if (pool == null) {
				pool = new SimpleJDBCConnectionPool("com.mysql.jdbc.Driver",
						"jdbc:mysql://185.82.20.252:3306", "Application",
						"notAtroll", 2, 10);
			}
			cn  = pool.reserveConnection();
			cn.setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return cn;

	}
	
	public static void releaseConnection(Connection cn){
		pool.releaseConnection(cn);
	}
}
