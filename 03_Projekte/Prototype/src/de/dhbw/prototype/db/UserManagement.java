package de.dhbw.prototype.db;

import java.sql.SQLException;

import de.dhbw.prototype.db.file.Folder;
import de.dhbw.prototype.db.user.User;

public interface UserManagement {

	public void createNewUser(User userToCreate) throws SQLException;

	public User getUserById(int id) throws SQLException;
	
	public User getUserByEmail(String email) throws SQLException;

	public void grantUserAcessToFolder(Folder folder, User... users) throws SQLException;

	public boolean loginUser(String email, String password) throws SQLException ;

	public void deleteUser(User userToDelete) throws SQLException;

	public void updateUser(User userToUpdate) throws SQLException;
}
