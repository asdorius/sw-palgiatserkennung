package de.dhbw.prototype.search;

import java.util.List;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

import de.dhbw.prototype.search.GoogleSearchResults.Result;

/**
 * 
 * @author Moritz Ragg Interface for Search-Classes
 */
public interface SearchEngine {

	public List<SearchResult> search(String phrase) throws IOException;

	public List<SearchResult> search(String phrase, int limit) throws IOException;
}
